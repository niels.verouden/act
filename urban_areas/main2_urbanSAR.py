import time
start_time = time.time()

# =============================================================================
# INFORMATION ABOUT THE SCRIPT
# =============================================================================
"""
_______________________________________________________________________________

INFO:
This script extracts the mean VV backscatter values from areas that are classed 
as 'built-up' in the Global Human Settlement Layer (GHSL). At the current moment,
a CSV file is exported that contains the mean VV backscatter values per date, as
well as the average precipitation per date and the sum of the last x days. Moreover
several outlier detection methods are done to find outliers in the dataset. The
outliers, which are linked to the dates in the dataset, are both plotted and exported
to the same CSV file as the mean VV backscatter. This CSV file is saved in the 
following directory -> 'Urban_areas/output/{file_name}/outlierDetection.CSV'

PARAMETERS:
For SAR_path, insert the name of the folder containing unzipped and stacked SAR 
files. All SAR files should be placed in the data folder and loaded according to
there corresponding path (i.e. "./Ndjamena_Jan2018Jun2022". For the urban_raster, 
the name of urban raster that should be loaded should be defined. In our case, 
all GHSL files are saved to a folder called 'urban_areas' inside the 'data' folder. 
Moreover,the name of the GHSL has been changed somewhat so it is easier to know 
which file should be imported (raster for Haiti -> "GHS_BUILT_S_E2018_GLOBE_R2022A_HAITI.tif")

A tuple of (xmin, ymin, xmax, and ymax) should be defined for the 'coordinates' 
variable (from for instance bboxfinder.com). This variable is used to mask the 
urban raster. It is also possible to leave the coordinates variable empty 
(i.e. assign an empty tuple). In this case, the exact extent of the SAR images 
will be taken. Be aware of the following: 
(1) If the bbox is larger than the SAR extent, all urban areas in the SAR extent 
will be taken, (2) if the bbox is smaller than the SAR extent, only the urban areas 
within the bbox will be taken (even though the SAR extent is larger). Advisable 
to check the bbox extent first before running the whole script (this is done by
only running STEP 1 of EXECUTE RASTER CLIPPING). 

There is also an option to show the extent of the boundingbox (from coordinates) 
on the first SAR image. It visualises how the boundingbox has been defined relative 
to the SAR extent. Finally, the file name of the precipitation csv should be defined. 
This file should be saved in the 'precipitation_data' folder inside the main 'data' 
folder. Do not forget to add '.CSV' behind the file name.

_______________________________________________________________________________

Source; https://ghsl.jrc.ec.europa.eu/documents/GHSL_Data_Package_2022.pdf?t=1655995832
_______________________________________________________________________________
"""

# =============================================================================
# IMPORT FUNCTIONS
# =============================================================================
from urban_areas.py.s21UrbanClipping import createBbox, maskUrban, rasterToPolygon, clipSAR, exportToCSV
from urban_areas.py.s22VisualiseData import visualiseData
from urban_areas.py.s23OutlierDetection import visualOutlierDetection, statisticalOutlierDetection, visualiseStatisticalOutliers, exportOutlierDetection

# =============================================================================
# DEFINE INPUT PARAMETERS (for more info, see above)
# =============================================================================
# PARAMETERS FOR RASTER CLIPPING
# Make 'coordinates' an empty tuple to take SAR image as extent 
SAR_path = 'Ndjamena_Jan2018Jun2022'
urban_raster = 'urban_areas/GHS_BUILT_S_E2018_GLOBE_R2022A_CHAD.tif' 
coordinates = (14.992973,12.052665,15.138199,12.203795)

# Show boundingbox in map? (y/n) (not exported to output)
show_bbox = 'y'

# =============================================================================
# PARAMETERS FOR VISUALISATION
# Add file name of the precipitation data that should be linked to the mean vv (including '.CSV')
precipitation_csv = 'precipitation_data/daily_precipitation_avg14days_Ndjamena_Jan2018Jun2022.CSV'

# =============================================================================
# EXECUTE RASTER CLIPPING
# =============================================================================
# STEP 1: CREATE BOUNDING BOX AROUND URBAN AREA
bbox = createBbox(coordinates, SAR_path, show_bbox)

# STEP 2: MASK URBAN RASTER BY BBOX
temp_path_masked = maskUrban(urban_raster, bbox)

# STEP 3: CONVERT MASKED RASTER TO POLYGON
gpd_urban = rasterToPolygon(temp_path_masked, SAR_path)

# STEP 4: CLIP POLYGON WITH SAR IMAGES
mean_values, dates = clipSAR(SAR_path, gpd_urban, coordinates)

# STEP 5: EXPORT MEAN VALUES AS CSV FILE
file_name, path_mean_vv = exportToCSV(mean_values, dates, SAR_path)

# =============================================================================
# VISUALISE RESULTS AND OUTLIER DETECTION
# =============================================================================
# STEP 1: LINEPLOTS OF MEAN VV BACKSCATTER, AVG PRECIPITATION, AND SUM PRECIPITATION
df_total, days, mean_VV, sum_xdays, title_name = visualiseData(SAR_path, precipitation_csv, file_name, path_mean_vv)

# STEP 2: VISUAL OUTLIER DETECTION
output_dir = visualOutlierDetection(title_name, df_total, days, mean_VV, sum_xdays)

# STEP 3: STATISTICAL OUTLIER DETECTION
df_outlier, LR_columns = statisticalOutlierDetection(df_total, mean_VV, sum_xdays)
visualiseStatisticalOutliers(output_dir, title_name, df_outlier, LR_columns, mean_VV, sum_xdays, days)

# STEP 4: EXPORT OUTLIER DETECTION DF
exportOutlierDetection(output_dir, df_outlier, title_name)


print(f"----- {round((time.time() - start_time), 2)} seconds -----")
