# # =============================================================================
# # STEP 7: REWRITE TIF NAMES WITH CORRECT DATES
# # =============================================================================
import rasterio as rio
import numpy as np
import os

def rewriteTIF(eopatch, file_name, vv_path, vh_path, dem_path):
    # Define data within eopatch and write to new list
    timestamp_date = eopatch['timestamp']
    timestamp = []
    for i in timestamp_date:
        timestamp.append(i.strftime("%Y-%m-%d"))
    
    # Open VV data, as well as metadata
    with rio.open(vv_path, 'r') as dst:
        vv_data = dst.read()
        vv_data = vv_data.astype(rio.float64)
        meta = dst.meta
        meta.update(count=4)
    
    # Open VH data
    with rio.open(vh_path, 'r') as dst:
        vh_data = dst.read()
        vh_data = vh_data.astype(rio.float32)
    
    # Open DEM data
    with rio.open(dem_path, 'r') as dst:
        dem = dst.read()
        
    #Scale to range 0-1    
    vv_data /= np.amax(vv_data)
    vh_data /= np.amax(vh_data)
    
    #Calculate an index that highlights water bodies as third band:
    vvvh_sum = np.add(vv_data,vh_data)
    vvvh_dif = np.subtract(vv_data,vh_data)
    vvvh_index = np.divide(vvvh_dif,vvvh_sum,out=np.zeros_like(vvvh_sum), where=vvvh_sum!=0)

    # Zip VV, VH, and VV/VH arrays together into one array
    stack = [[x]+[y]+[z] for x, y, z in zip(vv_data, vh_data, vvvh_index)]
    
    names = []
    # Write arrays to tif based on their data
    for i in range(len(timestamp)):
        stack_filename="%s_vv_vh_vvvhratio_Stack.tiff"%timestamp[i]   
        stack_filepath= os.path.join('data', file_name, stack_filename)
        with rio.open(stack_filepath, "w",**meta) as dst:
            dst.write_band(1, stack[i][0])
            dst.write_band(2, stack[i][1])
            dst.write_band(3, stack[i][2])
            dst.write_band(4, dem[0])
            dst.descriptions = tuple(['VV','VH','VV/VH_index','DEM'])
            names.append(stack_filename)
    
    # Remove temporary VV and VH files
    os.remove(vv_path)
    os.remove(vh_path)
    os.remove(dem_path)

    return names
