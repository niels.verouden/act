# =============================================================================
# STEP 8: VISUALISE RESULTS
# =============================================================================
import rasterio as rio
from rasterio import plot
import matplotlib.pyplot as plt
import os

def visualiseResults(visualise, bands, names, file_name):
    if visualise == 'y':
        for i in range(len(bands)):
            if bands[i] == 'VV':
                band = 0
            elif bands[i] == 'VH':
                band = 1
            elif bands[i] == 'VVVH':
                band = 2
            elif bands[i] == 'DEM':
                band = 3
            
            # Plot all images for each band in bands, expect for DEM (only plot DEM once)
            if band != 3:
                for x in range(len(names)):
                    path_stacked = os.path.join('data', file_name, names[x])
                    SAR = rio.open(path_stacked)
                    SARi = SAR.read(band+1)
            
                    date = names[x][0:10]
                    fig, ax = plt.subplots(figsize=(10,10))
                    rio.plot.show(SARi, ax=ax)
                    plt.title(f'{bands[i]} {date}', fontdict={'fontsize': 20})
                    plt.axis(False)
                    
                    del SAR
            else:
                path_stacked = os.path.join('data', file_name, names[0])
                SAR = rio.open(path_stacked)
                SARi = SAR.read(band+1)
        
                date = names[0][0:10]
                fig, ax = plt.subplots(figsize=(10,10))
                rio.plot.show(SARi, ax=ax)
                plt.title(f'{bands[i]} {date}', fontdict={'fontsize': 20})
                plt.axis(False)
                
                del SAR
    
